import React from "react";
import ProxyListPage from "./page/ProxyListPage";
import {ThemeProvider} from '@material-ui/styles';
import {createMuiTheme} from "@material-ui/core";
import {useGlobal} from "./GlobalHook";

const App = () => {
    const [globalState, globalActions] = useGlobal();
    const theme = createMuiTheme({
        palette: {
            type: globalState.darkMode ? "dark" : "light",
        },
    });

    return <ThemeProvider theme={theme}>
        <ProxyListPage/>
    </ThemeProvider>;
};

export default App;
