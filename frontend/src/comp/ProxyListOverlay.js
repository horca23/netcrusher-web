import React, {useState} from 'react';

import clsx from 'clsx';

import Button from "@material-ui/core/Button";
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import AddCircleIcon from '@material-ui/icons/AddCircle';
import AppBar from '@material-ui/core/AppBar';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import CodeIcon from '@material-ui/icons/Code';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import MenuIcon from '@material-ui/icons/Menu';
import PauseIcon from '@material-ui/icons/Pause';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';

import {makeStyles} from '@material-ui/core/styles';
import {closeAllProxies, freezeAllProxies, openAllProxies, removeAllProxies} from "../api/rest";
import {useGlobal} from "../GlobalHook";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import {Modal} from "@material-ui/core";
import Backdrop from '@material-ui/core/Backdrop';
import {NewProxyModal} from "./NewProxyModal";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(7) + 1,
        },
    },
    title: {
        flexGrow: 1,
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

export default function ProxyListOverlay(props) {
    const classes = useStyles();
    const [drawerOpen, setDrawerOpen] = useState(true);
    const [newOpen, setNewOpen] = useState(false);

    function handleDrawerOpen() {
        setDrawerOpen(true);
    }

    function handleDrawerClose() {
        setDrawerOpen(false);
    }

    const [globalState, globalActions] = useGlobal();

    return (
        <div className={classes.root}>
            <NewProxyModal open={newOpen} setOpen={setNewOpen} />
            <CssBaseline/>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: drawerOpen,
                })}
                elevation={0}
                color={"inherit"}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: drawerOpen,
                        })}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" noWrap className={classes.title}>
                        Netcrusher
                    </Typography>
                    <Tooltip title="Switch theme" placement="bottom" enterDelay={300}>
                        <Button
                            color="inherit"
                            onClick={() => globalActions.switchTheme()}
                        >
                            <Brightness4Icon/>
                        </Button>
                    </Tooltip>
                    <Tooltip title="Source code" placement="bottom" enterDelay={300}>
                        <Button
                            color="inherit"
                            onClick={() => window.open("https://gitlab.com/horca23/netcrusher-web", "_blank")}
                        >
                            <CodeIcon/>
                        </Button>
                    </Tooltip>
                </Toolbar>
                <Divider/>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: drawerOpen,
                    [classes.drawerClose]: !drawerOpen,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: drawerOpen,
                        [classes.drawerClose]: !drawerOpen,
                    }),
                }}
                open={drawerOpen}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>
                <Divider/>
                <List>
                    <ListItem button onClick={() => setNewOpen(true)} key="Add new proxy">
                        <ListItemIcon><AddCircleIcon/></ListItemIcon>
                        <ListItemText primary="Add new proxy"/>
                    </ListItem>
                </List>
                <Divider/>
                <List>
                    <ListSubheader inset>Bulk operations</ListSubheader>
                    <ListItem button onClick={async () => openAllProxies()} key="Open all">
                        <ListItemIcon><PlayArrowIcon/></ListItemIcon>
                        <ListItemText primary="Open all"/>
                    </ListItem>
                    <ListItem button onClick={async () => freezeAllProxies()} key="Freeze all">
                        <ListItemIcon><PauseIcon/></ListItemIcon>
                        <ListItemText primary="Freeze all"/>
                    </ListItem>
                    <ListItem button onClick={async () => closeAllProxies()} key="Close all">
                        <ListItemIcon><StopIcon/></ListItemIcon>
                        <ListItemText primary="Close all"/>
                    </ListItem>
                    <ListItem button onClick={async () => removeAllProxies()} key="Remove all">
                        <ListItemIcon><DeleteForeverIcon/></ListItemIcon>
                        <ListItemText primary="Remove all"/>
                    </ListItem>
                </List>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                {props.children}
            </main>
        </div>
    );
}