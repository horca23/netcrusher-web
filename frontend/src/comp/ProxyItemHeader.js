import React from 'react';

import {makeStyles} from '@material-ui/core/styles';

import Grid from "@material-ui/core/Grid"
import Button from "@material-ui/core/Button"

import PlayIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import StopIcon from '@material-ui/icons/Stop';
import {Typography} from "@material-ui/core";
import {closeProxy, freezeProxy, openProxy} from "../api/rest";
import clsx from "clsx";
import {useGlobal} from "../GlobalHook";

const useStyles = makeStyles(theme => ({
    grid: {
        padding: theme.spacing(1.5, 2, 1., 2),
        fontSize: theme.typography.pxToRem(15),
    },
    headerIcon: {
        fontSize: 15,
        margin: theme.spacing(0, 1)
    },
    operationIcon: {
        fontSize: 15,
    },
    iconsGridItem: {
        textAlign: "right"
    },
    openBg: {
        backgroundColor: "#e9fae9"
    },
    openBgDark: {
        backgroundColor: "#586860"
    },
    frozenBg: {
        backgroundColor: "#fefce0"
    },
    frozenBgDark: {
        backgroundColor: "#7f7b5c",
    },
    closedBg: {
        backgroundColor: "#ffebeb"
    },
    closedBgDark: {
        backgroundColor: "#7f5b5b"
    }
}));

const getHeaderStateIcon = (state, classes) => {
    switch (state) {
        case "OPEN":
            return <PlayIcon className={classes.headerIcon}/>;
        case "FROZEN":
            return <PauseIcon className={classes.headerIcon}/>;
        case "CLOSED":
            return <StopIcon className={classes.headerIcon}/>;
        default:
            throw new Error("Unexpected proxy state: " + state);
    }
};

export default function ProxyItemHeader(props) {
    const classes = useStyles();

    const conn = props.proxy.connection;
    const state = props.proxy.state;

    const [globalState, globalActions] = useGlobal();

    return (
        <Grid container className={
            clsx(
                classes.grid,
                globalState.darkMode
                    ? {
                        [classes.openBgDark]: state === "OPEN",
                        [classes.frozenBgDark]: state === "FROZEN",
                        [classes.closedBgDark]: state === "CLOSED",
                    }
                    : {
                        [classes.openBg]: state === "OPEN",
                        [classes.frozenBg]: state === "FROZEN",
                        [classes.closedBg]: state === "CLOSED",
                    }
            )
        }>
            <Grid item xs={6}>
                <Typography variant="subtitle1">
                    {"localhost:" + conn.bindPort}
                    {getHeaderStateIcon(state, classes)}
                    {conn.connectHost + ":" + conn.connectPort}
                </Typography>
            </Grid>
            <Grid item xs={6} className={classes.iconsGridItem}>
                <Button size="small" disabled={state === "OPEN"} onClick={async () => openProxy(conn.bindPort)}>
                    <PlayIcon className={classes.operationIcon}/>
                </Button>
                <Button size="small" disabled={state === "FROZEN"} onClick={async () => freezeProxy(conn.bindPort)}>
                    <PauseIcon className={classes.operationIcon}/>
                </Button>
                <Button size="small" disabled={state === "CLOSED"} onClick={async () => closeProxy(conn.bindPort)}>
                    <StopIcon className={classes.operationIcon}/>
                </Button>
                {/*<Button size="small" onClick={async () => removeProxy(conn.bindPort)}>*/}
                {/*    <TuneIcon className={classes.operationIcon}/>*/}
                {/*</Button>*/}
            </Grid>
        </Grid>
    )

}