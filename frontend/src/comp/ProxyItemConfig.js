import React, {useState} from 'react';

import {makeStyles} from '@material-ui/core/styles';
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {editConn, removeProxy} from "../api/rest";

const useStyles = makeStyles(theme => ({
    main: {
        padding: theme.spacing(2, 2)
    },
    container: {
        marginTop: theme.spacing(1),
        display: 'flex',
        flexWrap: 'wrap',
    },
    inputField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        paddingLeft: theme.spacing(0.5),
        paddingRight: theme.spacing(0.5)
    },
    button: {
        margin: theme.spacing(1, 0.5, 1, 0.5),
        float: "right"
    },
}));

export default function ProxyItemConfig(props) {
    const bindPort = props.proxy.connection.bindPort;

    const classes = useStyles();
    const [edited, setEdited] = useState(false);
    const [conn, setConn] = useState(props.proxy.connection);

    return (
        <div className={classes.main}>
            <Typography variant={"subtitle1"}>
                Connection
            </Typography>
            <Grid container>
                <Grid item xs={9}>
                    <TextField
                        type={"number"}
                        label={"Bind port"}
                        margin={"none"}
                        className={classes.inputField}
                        value={conn.bindPort}
                        onChange={newValue => {
                            setConn({...conn, bindPort: newValue.target.value});
                            setEdited(true)
                        }}
                    />
                    <TextField
                        label={"Connect host"}
                        className={classes.inputField}
                        value={conn.connectHost}
                        onChange={newValue => {
                            setConn({...conn, connectHost: newValue.target.value});
                            setEdited(true)
                        }}
                    />
                    <TextField
                        type={"number"}
                        label={"Connect port"}
                        margin={"none"}
                        className={classes.inputField}
                        value={conn.connectPort}
                        onChange={newValue => {
                            setConn({...conn, connectPort: newValue.target.value});
                            setEdited(true)
                        }}
                    />
                </Grid>
                <Grid item xs={3}>
                    <Button
                        variant={"text"}
                        size={"small"}
                        className={classes.button}
                        onClick={() => {
                            removeProxy(bindPort);
                        }}
                    >
                        Remove
                    </Button>
                    <Button
                        variant={"text"}
                        size={"small"}
                        className={classes.button}
                        disabled={!edited}
                        onClick={() => {
                            editConn(bindPort, conn);
                        }}
                    >
                        Save
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}