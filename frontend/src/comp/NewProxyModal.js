import React, {useState} from "react";

import {Modal, Typography} from "@material-ui/core";
import Fade from "@material-ui/core/Fade";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {addProxy} from "../api/rest";

const useStyles = makeStyles(theme => ({
    paper: {
        position: "absolute",
        width: "60%",
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(5, 7.5, 7.5, 7.5),
    },
    inputField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        paddingLeft: theme.spacing(0.5),
        paddingRight: theme.spacing(0.5)
    },
}));

export const NewProxyModal = (props) => {
    const classes = useStyles();
    const [edited, setEdited] = useState(false);
    const [conn, setConn] = useState({});

    return (
        <Modal
            open={props.open}
            onClose={() => props.setOpen(false)}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={props.open}>
                <div style={{top: "10%", left: "20%"}} className={classes.paper}>
                    <Typography variant={"h5"}>
                        New proxy
                    </Typography>
                    <Grid container>
                        <div style={{flexGrow: 1}}>
                            <TextField
                                type={"number"}
                                label={"Bind port"}
                                margin={"none"}
                                className={classes.inputField}
                                value={conn.bindPort}
                                onChange={newValue => {
                                    setConn({...conn, bindPort: newValue.target.value});
                                    setEdited(true)
                                }}
                            />
                            <TextField
                                label={"Connect host"}
                                className={classes.inputField}
                                value={conn.connectHost}
                                onChange={newValue => {
                                    setConn({...conn, connectHost: newValue.target.value});
                                    setEdited(true)
                                }}
                            />
                            <TextField
                                type={"number"}
                                label={"Connect port"}
                                margin={"none"}
                                className={classes.inputField}
                                value={conn.connectPort}
                                onChange={newValue => {
                                    setConn({...conn, connectPort: newValue.target.value});
                                    setEdited(true)
                                }}
                            />
                        </div>
                        <Button
                            variant={"text"}
                            size={"small"}
                            disabled={!edited}
                            onClick={() => {
                                addProxy(conn);
                                setConn({});
                                props.setOpen(false);
                            }}
                        >
                            Save
                        </Button>
                    </Grid>
                </div>
            </Fade>
        </Modal>
    )
};