import React, {useState} from 'react';
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import ProxyItemHeader from "./ProxyItemHeader";
import ProxyItemFooter from "./ProxyItemFooter";
import ProxyItemConfig from "./ProxyItemConfig";
import {Collapse} from "@material-ui/core";

export default function ProxyItem(props) {
    const [editing, setEditing] = useState(false);
    return (
        <Paper square={true} elevation={1}>
            <ProxyItemHeader proxy={props.proxy}/>
            <Divider/>
            <Collapse in={editing}>
                <ProxyItemConfig proxy={props.proxy}/>
            </Collapse>
            {editing && <Divider/>}
            <div style={{cursor: "pointer"}} onClick={() => setEditing(!editing)}>
                <ProxyItemFooter proxy={props.proxy} editing={editing}/>
            </div>
        </Paper>
    )
}