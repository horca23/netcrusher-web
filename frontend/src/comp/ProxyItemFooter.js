import React from 'react';

import {makeStyles} from '@material-ui/core/styles';


import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
    footer: {
        padding: theme.spacing(1.25, 2, 1., 2)
    },
    footerCollapse: {
        fontSize: 13
    },
    footerUpDown: {
        paddingRight: theme.spacing(1),
        textAlign: "right",
        fontSize: 13,
    },
    footerUpDownIcon: {
        fontSize: 13
    }
}));

const kb = 1000;
const mb = kb * 1000;
const gb = mb * 1000;
const formatSpeed = (speed) => {
    if (speed < kb) {
        return speed + "B/s"
    } else if (speed < mb) {
        return (speed / kb).toFixed(2) + "KB/s"
    } else if (speed < gb) {
        return (speed / mb).toFixed(2) + "MB/s"
    } else {
        return (speed / gb).toFixed(2) + "GB/s"
    }
};

export default function ProxyItemFooter(props) {
    const classes = useStyles();
    const speed = props.proxy.speed;
    return (
        <Grid container className={classes.footer}>
            <Grid item xs={4} className={classes.footerCollapse}>
                {props.editing
                    ? <ExpandLessIcon className={classes.footerCollapse}/>
                    : <ExpandMoreIcon className={classes.footerCollapse}/>
                }
                {" "}
                {props.editing
                    ? "Hide settings"
                    : "Settings"
                }
            </Grid>
            <Grid item xs={8} className={classes.footerUpDown}>
                {<ArrowDownwardIcon className={classes.footerUpDownIcon}/>}
                {speed && formatSpeed(speed.down)}
                {" "}
                <ArrowUpwardIcon className={classes.footerUpDownIcon}/>
                {speed && formatSpeed(speed.up)}
            </Grid>
        </Grid>
    )
}