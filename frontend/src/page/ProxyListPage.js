import React, {useEffect, useState} from 'react';

import {Container, Typography} from "@material-ui/core";
import ProxyListOverlay from "../comp/ProxyListOverlay";
import ProxyItem from "../comp/ProxyItem";
import Grid from "@material-ui/core/Grid";
import {getAllProxies} from "../api/rest";
import {subscribe} from "../api/ws";

export default function ProxyListPage() {
    const [proxies, setProxies] = useState([]);

    useEffect(() => {
        const fetchProxies = async () => {
            const proxies = await getAllProxies();
            setProxies(proxies);
        };

        subscribe({
            onProxyAdd: newProxy => setProxies(proxies => [...proxies, newProxy]),
            onProxyOpen: bindPort => setProxies(proxies =>
                proxies.map(proxy =>
                    proxy.connection.bindPort === bindPort ? {...proxy, state: "OPEN"} : proxy
                )
            ),
            onProxyFreeze: bindPort => setProxies(proxies =>
                proxies.map(proxy =>
                    proxy.connection.bindPort === bindPort ? {...proxy, state: "FROZEN"} : proxy
                )
            ),
            onProxyClose: bindPort => setProxies(proxies =>
                proxies.map(proxy =>
                    proxy.connection.bindPort === bindPort ? {...proxy, state: "CLOSED"} : proxy
                )
            ),
            onProxyRemove: bindPort => setProxies(proxies => proxies.filter(proxy => proxy.connection.bindPort !== bindPort)),
            onProxyEdit: (bindPort, edited) => setProxies(proxies =>
                proxies.map(proxy =>
                    proxy.connection.bindPort === bindPort
                        ? {
                            connection: edited.connection,
                            downloadThrottling: edited.downloadThrottling,
                            uploadThrottling: edited.uploadThrottling,
                            state: edited.state,
                            persistCommunication: edited.persistCommunication
                        }
                        : proxy
                )
            ),
            onProxySpeedReport: (bindPort, bytesDown, bytesUp) => setProxies(proxies =>
                proxies.map(proxy =>
                    proxy.connection.bindPort === bindPort
                        ? {...proxy, speed: {down: bytesDown, up: bytesUp}}
                        : proxy
                )
            )
        });

        fetchProxies();
    }, []);

    return (
        <ProxyListOverlay>
            <Container>
                <Grid container spacing={3}>
                    {
                        proxies.length > 0
                            ? proxies.map(proxy => (
                                <Grid item xs={12} key={proxy.connection.bindPort}>
                                    <ProxyItem proxy={proxy}/>
                                </Grid>
                            ))
                            : <Grid item xs={12}>
                                <Typography align="center" variant="subtitle1" component="div" color="textSecondary">
                                    No proxy defined
                                </Typography>
                            </Grid>
                    }
                </Grid>
            </Container>
        </ProxyListOverlay>
    );
}