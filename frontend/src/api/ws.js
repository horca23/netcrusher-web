export const subscribe = (subscription) => {
    const protocol = window.location.protocol === "https:" ? "wss://" : "ws://";
    const host = window.location.host;
    const path = "/ws";
    const ws = new WebSocket(protocol + host + path)
    ws.onmessage = (event) => {
        const message = JSON.parse(event.data);
        const data = JSON.parse(message.data);
        switch (message.action) {
            case "ADD":
                subscription.onProxyAdd(data);
                break;
            case "OPEN":
                subscription.onProxyOpen(Number(data));
                break;
            case "FREEZE":
                subscription.onProxyFreeze(Number(data));
                break;
            case "CLOSE":
                subscription.onProxyClose(Number(data));
                break;
            case "REMOVE":
                subscription.onProxyRemove(Number(data));
                break;
            case "EDIT":
                subscription.onProxyEdit(data.bindPort, data.model);
                break;
            case "SPEED_REPORT":
                subscription.onProxySpeedReport(data.bindPort, data.bytesDown, data.bytesUp);
                break;
            default:
                console.warn("Unexpected websocket message operation: " + message.action);
        }
    }
};
