import {createBulkActionDto, createGranularProxyUpdateDto, createNewProxyDto} from "./dtos";

export const getAllProxies = async () => {
    let response = await handleHttpRequest("/api/proxy", "GET");
    return response.proxies;
};

export const addProxy = async (conn) => {
    await handleHttpRequest(
        "/api/proxy",
        "POST",
        createNewProxyDto(conn)
    )
};

export const openAllProxies = async () => {
    await handleHttpRequest(
        "/api/proxy",
        "PATCH",
        createBulkActionDto("OPEN_ALL")
    );
};

export const freezeAllProxies = async () => {
    await handleHttpRequest(
        "/api/proxy",
        "PATCH",
        createBulkActionDto("FREEZE_ALL")
    );
};

export const closeAllProxies = async () => {
    await handleHttpRequest(
        "/api/proxy",
        "PATCH",
        createBulkActionDto("CLOSE_ALL")
    );
};

export const openProxy = async (bindPort) => {
    await handleHttpRequest(
        `/api/proxy/${bindPort}`,
        "PATCH",
        createGranularProxyUpdateDto("OPEN")
    );
};

export const freezeProxy = async (bindPort) => {
    await handleHttpRequest(
        `/api/proxy/${bindPort}`,
        "PATCH",
        createGranularProxyUpdateDto("FREEZE")
    );
};

export const closeProxy = async (bindPort) => {
    await handleHttpRequest(
        `/api/proxy/${bindPort}`,
        "PATCH",
        createGranularProxyUpdateDto("CLOSE")
    );
};

export const editDescription = async (bindPort, newDescription) => {
    await handleHttpRequest(
        `/api/proxy/${bindPort}`,
        "PATCH",
        createGranularProxyUpdateDto("DESCRIPTION_EDIT", newDescription)
    )
};

export const editConn = async (bindPort, newConn) => {
    await handleHttpRequest(
        `/api/proxy/${bindPort}`,
        "PATCH",
        createGranularProxyUpdateDto("CONNECTION_EDIT", JSON.stringify(newConn))
    );
};

export const removeProxy = async (bindPort) => {
    await handleHttpRequest(`/api/proxy/${bindPort}`, "DELETE");
};

export const removeAllProxies = async () => {
    await handleHttpRequest("/api/proxy", "DELETE");
};

const handleHttpRequest = async (url, method, body) => {
    let response = await fetch(url, {
        method: method,
        body: body,
        headers: body ? {'Content-Type': 'application/json;charset=utf-8'} : {}
    });
    if (response.ok) {
        if (response.status === 200) {
            return response.json();
        }
    } else {
        let error = await response.text();
        alert(`Error from backend: '${error}'`)
    }
};