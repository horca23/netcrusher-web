import useGlobalHook from "use-global-hook";
import React from "react";

const initialState = {
    darkMode: false,
};

const actions = {
    switchTheme: (store) => {
        store.setState({darkMode: !store.state.darkMode});
    },
};

export const useGlobal = useGlobalHook(React, initialState, actions);