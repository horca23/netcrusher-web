package com.horcacorp.testing.netcrusherweb.webserver.ktor

import io.ktor.application.Application

interface KtorComponent {

    fun addComponent(app: Application)

}