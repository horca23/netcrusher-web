package com.horcacorp.testing.netcrusherweb.webserver.ktor.rest

import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyNotFoundException
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyService
import com.horcacorp.testing.netcrusherweb.webserver.ktor.KtorComponent
import com.horcacorp.testing.netcrusherweb.webserver.ktor.dto.*
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.util.error
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import org.slf4j.event.Level

class KtorRestComponent(private val proxyService: ProxyService) : KtorComponent {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val basePath = "/api/proxy"

    override fun addComponent(app: Application) {
        logger.info("Initializing Ktor REST API component on path '$basePath'.")
        app.apply {
            install(DefaultHeaders)
            install(CallLogging) {
                logger = this@KtorRestComponent.logger
                level = Level.INFO
            }
            install(Compression) {
                gzip()
            }
            install(ContentNegotiation) {
                jackson()
            }
            install(StatusPages) {
                exception<Exception> { e ->
                    logger.error(e)
                    call.respond(
                        when (e) {
                            is ProxyNotFoundException -> HttpStatusCode.NotFound
                            is IllegalArgumentException, is JsonMappingException -> HttpStatusCode.UnprocessableEntity
                            else -> HttpStatusCode.InternalServerError
                        }, e.message ?: ""
                    )
                }
            }
            routing {
                route(basePath) {
                    get {
                        call.respond(ProxyListDto(proxyService.getAll().map { it.toDto() }))
                    }
                    post {
                        proxyService.add(call.receive<ProxyDto>().toModel())
                        call.respond(HttpStatusCode.NoContent)
                    }
                    patch {
                        when (call.receive<ProxyBulkActionDto>().bulkAction) {
                            ProxyBulkAction.OPEN_ALL -> proxyService.openAll()
                            ProxyBulkAction.CLOSE_ALL -> proxyService.closeAll()
                            ProxyBulkAction.FREEZE_ALL -> proxyService.freezeAll()
                        }
                        call.respond(HttpStatusCode.NoContent)
                    }
                    delete {
                        proxyService.removeAll()
                        call.respond(HttpStatusCode.NoContent)
                    }
                    route("{bindPort}") {
                        patch {
                            handleGranularUpdate(call.receive(), getBindPort())
                            call.respond(HttpStatusCode.NoContent)
                        }
                        delete {
                            proxyService.remove(getBindPort())
                            call.respond(HttpStatusCode.NoContent)
                        }
                        route("history") {
                            get {
                                val bindPort = getBindPort()
                                call.respond(ProxyHistoryListDto(proxyService.getHistory(bindPort).map { it.toDto() }))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun PipelineContext<*, ApplicationCall>.getBindPort() = call.parameters["bindPort"]!!.toInt()

    private suspend fun handleGranularUpdate(granularUpdate: ProxyGranularUpdateDto, bindPort: Int) {
        when (granularUpdate.action) {
            ProxyAction.OPEN -> proxyService.open(bindPort)
            ProxyAction.CLOSE -> proxyService.close(bindPort)
            ProxyAction.FREEZE -> proxyService.freeze(bindPort)
            ProxyAction.DESCRIPTION_EDIT -> proxyService.editDescription(bindPort, granularUpdate.partialUpdate!!)
            ProxyAction.CONNECTION_EDIT -> jacksonObjectMapper()
                .readValue<ProxyConnectionDto>(granularUpdate.partialUpdate!!)
                .also {
                    proxyService.editConnection(
                        bindPort,
                        it.toModel()
                    )
                }
            ProxyAction.THROTTLING_EDIT -> jacksonObjectMapper()
                .readValue<ThrottlingPartialConfigDto>(granularUpdate.partialUpdate!!)
                .also {
                    proxyService.editThrottling(
                        bindPort,
                        it.downConfig.toModel(),
                        it.upConfig.toModel()
                    )
                }
            ProxyAction.PERSISTING_ON -> proxyService.turnOnCommunicationPersisting(bindPort)
            ProxyAction.PERSISTING_OFF -> proxyService.turnOffCommunicationPersisting(bindPort)
        }
    }
}