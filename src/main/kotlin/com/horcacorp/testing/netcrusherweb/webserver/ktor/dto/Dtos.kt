package com.horcacorp.testing.netcrusherweb.webserver.ktor.dto

import com.horcacorp.testing.netcrusherweb.proxy.api.*

data class ProxyListDto(
    val proxies: List<ProxyDto> // never send top level JSON array as a response
)

data class ProxyDto(
    val description: String,
    val connection: ProxyConnectionDto,
    val downloadThrottling: ThrottlingConfigDto,
    val uploadThrottling: ThrottlingConfigDto,
    val state: ProxyStateDto,
    val persistCommunication: Boolean
)

data class ProxyConnectionDto(
    val bindPort: Int,
    val connectHost: String,
    val connectPort: Int
)

enum class ProxyStateDto {
    OPEN, CLOSED, FROZEN
}

data class ThrottlingConfigDto(
    val value: Long,
    val mode: ThrottlingModeDto
)

enum class ThrottlingModeDto {
    OFF, BYTE_RATE, DELAY
}

data class ProxyBulkActionDto(
    val bulkAction: ProxyBulkAction
)

enum class ProxyBulkAction {
    OPEN_ALL,
    CLOSE_ALL,
    FREEZE_ALL,
}

data class ProxyGranularUpdateDto(
    val action: ProxyAction,
    val partialUpdate: String?
)

data class ThrottlingPartialConfigDto(
    val downConfig: ThrottlingConfigDto,
    val upConfig: ThrottlingConfigDto
)

enum class ProxyAction {
    OPEN,
    CLOSE,
    FREEZE,
    DESCRIPTION_EDIT,
    CONNECTION_EDIT,
    THROTTLING_EDIT,
    PERSISTING_ON,
    PERSISTING_OFF,
}

fun ProxyModel.toDto() = ProxyDto(
    description,
    connection.toDto(),
    downloadThrottling.toDto(),
    uploadThrottling.toDto(),
    state.toDto(),
    persistCommunication
)

fun ProxyConnection.toDto() = ProxyConnectionDto(bindPort, connectHost, connectPort)

fun ProxyState.toDto() = when (this) {
    ProxyState.OPEN -> ProxyStateDto.OPEN
    ProxyState.CLOSED -> ProxyStateDto.CLOSED
    ProxyState.FROZEN -> ProxyStateDto.FROZEN
}

fun ThrottlingConfig.toDto() = ThrottlingConfigDto(
    value,
    mode.toDto()
)

fun ThrottlingMode.toDto() = when (this) {
    ThrottlingMode.OFF -> ThrottlingModeDto.OFF
    ThrottlingMode.BYTE_RATE -> ThrottlingModeDto.BYTE_RATE
    ThrottlingMode.DELAY -> ThrottlingModeDto.DELAY
}


fun ProxyDto.toModel() = ProxyModel(
    description,
    connection.toModel(),
    downloadThrottling.toModel(),
    uploadThrottling.toModel(),
    state.toModel(),
    persistCommunication
)

fun ProxyConnectionDto.toModel() = ProxyConnection(bindPort, connectHost, connectPort)

fun ProxyStateDto.toModel() = when (this) {
    ProxyStateDto.OPEN -> ProxyState.OPEN
    ProxyStateDto.CLOSED -> ProxyState.CLOSED
    ProxyStateDto.FROZEN -> ProxyState.FROZEN
}

fun ThrottlingConfigDto.toModel() = ThrottlingConfig(
    value,
    mode.toModel()
)

fun ThrottlingModeDto.toModel() = when (this) {
    ThrottlingModeDto.OFF -> ThrottlingMode.OFF
    ThrottlingModeDto.BYTE_RATE -> ThrottlingMode.BYTE_RATE
    ThrottlingModeDto.DELAY -> ThrottlingMode.DELAY
}

data class ProxyHistoryListDto(
    val history: List<ProxyHistoryDto>
)

data class ProxyHistoryDto(
    val timestamp: Long,
    val type: ProxyHistoryTypeDto
)

enum class ProxyHistoryTypeDto {
    CREATED, OPENED, FROZEN, CLOSED, EDITED
}

fun ProxyHistory.toDto() = ProxyHistoryDto(
    timestamp,
    type.toDto()
)

fun ProxyHistoryType.toDto() = when (this) {
    ProxyHistoryType.CREATED -> ProxyHistoryTypeDto.CREATED
    ProxyHistoryType.OPENED -> ProxyHistoryTypeDto.OPENED
    ProxyHistoryType.FROZEN -> ProxyHistoryTypeDto.FROZEN
    ProxyHistoryType.CLOSED -> ProxyHistoryTypeDto.CLOSED
    ProxyHistoryType.EDITED -> ProxyHistoryTypeDto.EDITED
}