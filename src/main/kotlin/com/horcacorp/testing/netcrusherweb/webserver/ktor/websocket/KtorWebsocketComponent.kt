package com.horcacorp.testing.netcrusherweb.webserver.ktor.websocket

import com.horcacorp.testing.netcrusherweb.webserver.ktor.KtorComponent
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.pingPeriod
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.routing
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import org.slf4j.LoggerFactory
import java.time.Duration

class KtorWebsocketComponent(private val websocketResource: ProxyWebsocketResource) : KtorComponent {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val wsPath = "/ws"

    override fun addComponent(app: Application) {
        app.apply {
            logger.info("Initializing Ktor websocket component on path '$wsPath'.")
            install(WebSockets) {
                pingPeriod = Duration.ofSeconds(30)
            }
            routing {
                webSocket(wsPath) {
                    websocketResource.onOpen(this)
                    try {
                        for (frame in incoming) {
                            val message = (frame as Frame.Text).readText()
                            logger.info("Consuming websocket message '$message'.")
                        }
                        websocketResource.onExit(this)
                    } catch (e: ClosedReceiveChannelException) {
                        websocketResource.onExit(this)
                    } catch (e: Exception) {
                        logger.error("Client disconnected with error.", e)
                        websocketResource.onExit(this)
                    }
                }
            }
        }
    }

}