package com.horcacorp.testing.netcrusherweb.webserver.ktor.dto

data class ServerToClientWebsocketDto(
    val action: ServerToClientAction,
    val data: String
)

data class WebsocketProxyEditedDto(
    val bindPort: Int,
    val model: ProxyDto
)

data class WebsocketSpeedReportDto(
    val bindPort: Int,
    val bytesDown: Int,
    val bytesUp: Int
)

enum class ServerToClientAction {
    ADD,
    OPEN,
    FREEZE,
    CLOSE,
    REMOVE,
    EDIT,
    SPEED_REPORT
}