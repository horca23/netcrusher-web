package com.horcacorp.testing.netcrusherweb.persistance.api

import java.io.File

interface ManagedFilesMutexPool {

    suspend fun <T> borrowAndReturn(file: File, func: () -> T): T
    suspend fun borrow(file: File)
    suspend fun `return`(file: File)
}