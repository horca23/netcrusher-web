package com.horcacorp.testing.netcrusherweb.persistance.api

import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyModel

interface ProxyConfigPersistor {

    suspend fun getProxies(): List<ProxyModel>
    suspend fun saveProxies(proxies: List<ProxyModel>)

}