package com.horcacorp.testing.netcrusherweb.persistance.impl

import com.horcacorp.testing.netcrusherweb.persistance.api.ManagedFilesMutexPool
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.io.File

// todo d.bydzovsky Performance: Rewrite this to actor.. To not use a mutex at all
class ManagedFilesMutexPoolImpl : ManagedFilesMutexPool {
    private val locks = mutableMapOf<String, Mutex>()
    private val masterLock = Mutex(locked = false)

    override suspend fun <T> borrowAndReturn(file: File, func: () -> T): T {
        val fileLock = masterLock.withLock {
            return@withLock locks.computeIfAbsent(file.canonicalPath) { Mutex(locked = false) }
        }
        return fileLock.withLock { func() }
    }

    override suspend fun borrow(file: File) {
        val fileLock = masterLock.withLock {
            return@withLock locks.computeIfAbsent(file.canonicalPath) { Mutex(locked = false) }
        }
        fileLock.lock()
    }

    override suspend fun `return`(file: File) {
        val fileLock = masterLock.withLock {
            return@withLock locks.computeIfAbsent(file.canonicalPath) { Mutex(locked = false) }
        }
        fileLock.unlock()
    }
}