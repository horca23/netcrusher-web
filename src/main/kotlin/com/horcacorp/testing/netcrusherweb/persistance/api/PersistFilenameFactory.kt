package com.horcacorp.testing.netcrusherweb.persistance.api

import java.io.File

interface PersistFilenameFactory {

    fun getConfigFile(): File
    fun getUploadTrafficFile(bindPort: Int): File
    fun getDownloadTrafficFile(bindPort: Int): File
    fun getHistoryFile(bindPort: Int): File

}