package com.horcacorp.testing.netcrusherweb.persistance.impl

import com.horcacorp.testing.netcrusherweb.persistance.api.ManagedFilesMutexPool
import com.horcacorp.testing.netcrusherweb.persistance.api.ProxyHistoryPersistor
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyHistory
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyHistoryType
import com.horcacorp.testing.netcrusherweb.persistance.api.PersistFilenameFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class FileSystemProxyHistoryPersistor(
    private val filesPool: ManagedFilesMutexPool,
    private val filenameFactory: PersistFilenameFactory
) : ProxyHistoryPersistor {

    private val logger = LoggerFactory.getLogger(javaClass)

    override suspend fun getHistory(bindPort: Int): List<ProxyHistory> {
        return withContext(Dispatchers.IO) {
            val historyFile = filenameFactory.getHistoryFile(bindPort)
            filesPool.borrowAndReturn(historyFile) {
                if (historyFile.exists()) {
                    historyFile.readLines().map {
                        val values = it.split(",")
                        ProxyHistory(values[0].toLong(), ProxyHistoryType.valueOf(values[1]))
                    }
                } else {
                    emptyList()
                }
            }
        }
    }

    override suspend fun saveHistory(bindPort: Int, item: ProxyHistory) {
        withContext(Dispatchers.IO) {
            try {
                val historyFile = filenameFactory.getHistoryFile(bindPort)
                filesPool.borrowAndReturn(historyFile) {
                    if (!historyFile.exists()) {
                        historyFile.createNewFile()
                    }
                    historyFile.appendText("${item.timestamp},${item.type}\n")
                }
            } catch (e: Exception) {
                logger.error("Cannot persist proxy history.", e)
            }
        }
    }

    override suspend fun moveHistory(oldBindPort: Int, newBindPort: Int) {
        withContext(Dispatchers.IO) {
            try {
                val oldHistoryFile = filenameFactory.getHistoryFile(oldBindPort)
                val newHistoryFile = filenameFactory.getHistoryFile(newBindPort)
                filesPool.borrowAndReturn(oldHistoryFile) {
                    if (oldHistoryFile.exists()) {
                        Files.move(
                            oldHistoryFile.toPath(),
                            newHistoryFile.toPath(),
                            StandardCopyOption.REPLACE_EXISTING
                        )
                    }
                }
            } catch (e: Exception) {
                logger.error("Cannot move proxy history.", e)
            }
        }
    }

    override suspend fun removeHistory(bindPort: Int) {
        withContext(Dispatchers.IO) {
            try {
                val historyFile = filenameFactory.getHistoryFile(bindPort)
                filesPool.borrowAndReturn(historyFile) {
                    if (historyFile.exists()) {
                        historyFile.delete()
                    }
                }
            } catch (e: Exception) {
                logger.error("Cannot remove proxy history.", e)
            }
        }
    }
}