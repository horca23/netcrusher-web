package com.horcacorp.testing.netcrusherweb.persistance.impl

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.horcacorp.testing.netcrusherweb.persistance.api.ManagedFilesMutexPool
import com.horcacorp.testing.netcrusherweb.persistance.api.ProxyConfigPersistor
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyModel
import com.horcacorp.testing.netcrusherweb.persistance.api.PersistFilenameFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory

class FileSystemProxyConfigPersistor(
    private val filesPool: ManagedFilesMutexPool,
    private val filenameFactory: PersistFilenameFactory
) : ProxyConfigPersistor {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val mapper = jacksonObjectMapper()

    override suspend fun getProxies(): List<ProxyModel> {
        return withContext(Dispatchers.IO) {
            val file = filenameFactory.getConfigFile()
            filesPool.borrowAndReturn(file) {
                if (file.exists()) {
                    mapper.readValue(file.inputStream())
                } else {
                    emptyList<ProxyModel>()
                }
            }
        }
    }

    override suspend fun saveProxies(proxies: List<ProxyModel>) {
        withContext(Dispatchers.IO) {
            try {
                val file = filenameFactory.getConfigFile()
                filesPool.borrowAndReturn(file) {
                    if (!file.exists()) {
                        file.createNewFile()
                    }
                    file.writeBytes(
                        mapper
                            .writerWithDefaultPrettyPrinter()
                            .writeValueAsBytes(proxies)
                    )
                }
            } catch (e: Exception) {
                logger.error("Cannot persist proxy configuration.", e)
            }
        }
    }
}