package com.horcacorp.testing.netcrusherweb.proxy.api

class ProxyNotFoundException(bindPort: Int) : RuntimeException("Proxy with bind port '$bindPort' not found.")