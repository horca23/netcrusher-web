package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

data class SimpleTrafficItem(val metadata: TrafficMetadata,
                             val body: String)

data class TrafficMetadata(val nothingYet: String)