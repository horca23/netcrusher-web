package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

import com.horcacorp.testing.netcrusherweb.persistance.api.ManagedFilesMutexPool
import com.horcacorp.testing.netcrusherweb.proxy.api.TrafficReader
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher.EnrichedTrafficReader
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher.TrafficEnricherFactory
import java.io.File

class TrafficReaderFactory(private val managedFilesMutexPool: ManagedFilesMutexPool,
                           private val trafficEnricherFactory: TrafficEnricherFactory) {

    fun createReader(upFile: File, downFile: File): TrafficReader {
        val poorReader = CachedPoorTrafficReader(RollingFileFilterPoorTrafficReader(managedFilesMutexPool, upFile = upFile, downFile = downFile))
        return EnrichedTrafficReader(poorReader, trafficEnricherFactory.create())
    }
}