package com.horcacorp.testing.netcrusherweb.proxy.api

import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.ReadTrafficFilter
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.SimpleTrafficItem

interface TrafficReader {

    suspend fun readUpTraffic(filter: ReadTrafficFilter = ReadTrafficFilter()): Collection<EnrichedTrafficItem>
    suspend fun readDownTraffic(filter: ReadTrafficFilter = ReadTrafficFilter()): Collection<EnrichedTrafficItem>

}