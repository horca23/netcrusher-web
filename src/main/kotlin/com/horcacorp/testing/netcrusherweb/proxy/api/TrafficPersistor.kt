package com.horcacorp.testing.netcrusherweb.proxy.api

import java.io.File
import java.nio.ByteBuffer

interface TrafficPersistor {

    /**
     * Starts consuming from event bus and persist communication
     */
    fun initialize()

}
