package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

import java.util.stream.Stream


interface PoorTrafficReader {

    suspend fun getUpTrafficStream(filter: ReadTrafficFilter = ReadTrafficFilter()): Stream<SimpleTrafficItem?>

    suspend fun getDownTrafficStream(filter: ReadTrafficFilter = ReadTrafficFilter()): Stream<SimpleTrafficItem?>

}