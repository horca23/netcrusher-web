package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.writer

import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficItemPassed
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.rolling.RollingFile
import java.io.File

class RollingTrafficWriter(private val decorated: TrafficWriter) : TrafficWriter {

    override fun write(file: File, proxyItem: ProxyTrafficItemPassed) {
        val recent = RollingFile(file).incrementFilesAndGetRecent()
        decorated.write(recent, proxyItem)
    }
}