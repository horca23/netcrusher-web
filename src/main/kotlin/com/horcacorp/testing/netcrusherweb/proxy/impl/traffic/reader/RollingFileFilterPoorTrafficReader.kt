package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

import com.horcacorp.testing.netcrusherweb.persistance.api.ManagedFilesMutexPool
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.rolling.RollingFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.stream.Stream

class RollingFileFilterPoorTrafficReader(
    private val managedFilesMutexPool: ManagedFilesMutexPool,
    private val upFile: File,
    private val downFile: File
) : PoorTrafficReader {

    private val rollingUpFile = RollingFile(upFile)
    private val rollingDownFile = RollingFile(downFile)

    /**
     * Caller must close stream
     */
    override suspend fun getUpTrafficStream(filter: ReadTrafficFilter): Stream<SimpleTrafficItem?> {
        return readTraffic(upFile, rollingUpFile).addFilter(filter)
    }

    override suspend fun getDownTrafficStream(filter: ReadTrafficFilter): Stream<SimpleTrafficItem?> {
        return readTraffic(downFile, rollingDownFile).addFilter(filter)
    }

    private fun Stream<SimpleTrafficItem?>.addFilter(filter: ReadTrafficFilter) = apply {
        // todo d.bydzovsky implement filtering according to some traffic ID
        // not sure if new stream created by limit method closes underlying stream..
        limit(filter.limit.toLong()).onClose { this.close() }
    }

    private suspend fun readTraffic(file: File, rollingFile: RollingFile): Stream<SimpleTrafficItem?> {
        managedFilesMutexPool.borrow(file)
        val files = rollingFile.collectFilesAsc()
        if (files.isEmpty()) return Stream.empty()
        val maxIndex = files.size - 1
        var index = 0
        var currentStream = SimpleTrafficReader(files.elementAt(index)).trafficStream()
        var currentIterator = currentStream.iterator()

        return Stream.generate {
            if (currentIterator.hasNext()) {
                return@generate currentIterator.next()
            } else {
                currentStream.close()
                index++
                if (index > maxIndex) {
                    return@generate null
                } else {
                    currentStream = SimpleTrafficReader(files.elementAt(index)).trafficStream()
                    currentIterator = currentStream.iterator()
                    if (currentIterator.hasNext()) {
                        return@generate currentIterator.next()
                    } else {
                        return@generate null
                    }
                }
            }
        }.onClose {
            currentStream.close()
            CoroutineScope(Dispatchers.Default).launch { managedFilesMutexPool.`return`(file) }
        }
    }
}