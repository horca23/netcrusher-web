package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.writer

import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficItemPassed
import java.io.File

interface TrafficWriter {
    /**
     * The file is already locked with ManagerFilesMutexPool when this method is called.
     */
    fun write(file: File, proxyItem: ProxyTrafficItemPassed)
}