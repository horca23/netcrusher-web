package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

class ReadTrafficFilter {

    var limit = 100
    // null means that it takes first messages
    var id: String? = null

    // todo d.bydzovsky make the filter working...
    fun limit(count: Int) = apply {
        this.limit = limit
    }

    fun from(id: String) = {
        this.id = id
    }

}