package com.horcacorp.testing.netcrusherweb.proxy.api

data class ProxyHistory(
    val timestamp: Long,
    val type: ProxyHistoryType
)

enum class ProxyHistoryType {
    CREATED, OPENED, FROZEN, CLOSED, EDITED
}