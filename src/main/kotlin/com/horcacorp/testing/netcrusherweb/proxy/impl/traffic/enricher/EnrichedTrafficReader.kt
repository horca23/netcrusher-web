package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher

import com.horcacorp.testing.netcrusherweb.proxy.api.EnrichedTrafficItem
import com.horcacorp.testing.netcrusherweb.proxy.api.TrafficReader
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.PoorTrafficReader
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.ReadTrafficFilter
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.SimpleTrafficItem
import java.util.stream.Stream

class EnrichedTrafficReader(
    private val poorTrafficReader: PoorTrafficReader,
    private val trafficEnricher: TrafficEnricher
): TrafficReader {

    override suspend fun readUpTraffic(filter: ReadTrafficFilter)
            = poorTrafficReader.getUpTrafficStream(filter).readParseAndClose()


    override suspend fun readDownTraffic(filter: ReadTrafficFilter)
            = poorTrafficReader.getDownTrafficStream(filter).readParseAndClose()

    private fun Stream<SimpleTrafficItem?>.readParseAndClose(): Collection<EnrichedTrafficItem> {
        val result = mutableListOf<EnrichedTrafficItem>()
        val iterator = iterator()
        try {
            while(iterator.hasNext()) {
                result.add(trafficEnricher.enrich(iterator.next()!!))
            }
            return result
        } finally {
            this.close()
        }
    }
}