package com.horcacorp.testing.netcrusherweb.proxy.impl.history

import com.horcacorp.testing.netcrusherweb.event.api.*
import com.horcacorp.testing.netcrusherweb.persistance.api.ProxyHistoryPersistor
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyHistory
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyHistoryType
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyHistoryWriter

class ProxyHistoryWriterImpl(
    private val eventBus: EventBus,
    private val proxyHistoryPersistor: ProxyHistoryPersistor
) : ProxyHistoryWriter {

    override fun initialize() {
        eventBus.subscribe(ProxyAdded::class, ::onProxyCreated)
        eventBus.subscribe(ProxyOpened::class, ::onProxyOpen)
        eventBus.subscribe(ProxyFroze::class, ::onProxyFreeze)
        eventBus.subscribe(ProxyClosed::class, ::onProxyClose)
        eventBus.subscribe(ProxyEdited::class, ::onProxyEdit)
        eventBus.subscribe(ProxyRemoved::class, ::onProxyRemove)
    }

    override fun destroy() {
        eventBus.unsubscribe(ProxyAdded::class, ::onProxyCreated)
        eventBus.unsubscribe(ProxyOpened::class, ::onProxyOpen)
        eventBus.unsubscribe(ProxyFroze::class, ::onProxyFreeze)
        eventBus.unsubscribe(ProxyClosed::class, ::onProxyClose)
        eventBus.unsubscribe(ProxyEdited::class, ::onProxyEdit)
        eventBus.unsubscribe(ProxyRemoved::class, ::onProxyRemove)
    }

    private suspend fun onProxyCreated(event: ProxyAdded) {
        proxyHistoryPersistor.saveHistory(
            event.proxyModel.connection.bindPort,
            ProxyHistory(System.currentTimeMillis(), ProxyHistoryType.CREATED)
        )
    }

    private suspend fun onProxyOpen(event: ProxyOpened) {
        proxyHistoryPersistor.saveHistory(
            event.bindPort,
            ProxyHistory(System.currentTimeMillis(), ProxyHistoryType.OPENED)
        )
    }

    private suspend fun onProxyFreeze(event: ProxyFroze) {
        proxyHistoryPersistor.saveHistory(
            event.bindPort,
            ProxyHistory(System.currentTimeMillis(), ProxyHistoryType.FROZEN)
        )
    }

    private suspend fun onProxyClose(event: ProxyClosed) {
        proxyHistoryPersistor.saveHistory(
            event.bindPort,
            ProxyHistory(System.currentTimeMillis(), ProxyHistoryType.CLOSED)
        )
    }

    private suspend fun onProxyEdit(event: ProxyEdited) {
        val oldBindPort = event.bindPort
        val newBindPort = event.newProxyModel.connection.bindPort
        if (oldBindPort != newBindPort) {
            proxyHistoryPersistor.moveHistory(oldBindPort, newBindPort)
        }
        proxyHistoryPersistor.saveHistory(
            newBindPort,
            ProxyHistory(System.currentTimeMillis(), ProxyHistoryType.EDITED)
        )
    }

    private suspend fun onProxyRemove(event: ProxyRemoved) {
        proxyHistoryPersistor.removeHistory(event.bindPort)
    }

}