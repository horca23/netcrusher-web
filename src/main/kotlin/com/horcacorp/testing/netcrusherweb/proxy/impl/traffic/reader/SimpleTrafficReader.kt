package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.PersistTrafficDivider
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.TrafficConstant
import java.io.File
import java.lang.StringBuilder
import java.util.function.Supplier
import java.util.stream.Stream
import kotlin.streams.asSequence

class SimpleTrafficReader(private val file: File) {
    /**
     * File must be locked by caller
     */
    private val persistDivider = PersistTrafficDivider()

    /**
     * Caller has to close a stream when caller does not reach end of stream (null)
     */
    fun trafficStream(): Stream<SimpleTrafficItem?> {
        val reader = file.bufferedReader()
        val lineIterator = reader.lines().iterator()

        return Stream.generate {
            readTrafficItem(lineIterator).also {
                if (it == null) reader.close()
            }
        }.onClose { reader.close() }
    }

    private fun readTrafficItem(lineIterator: Iterator<String>): SimpleTrafficItem? {
        if (lineIterator.hasNext()) {
            val body = StringBuilder()
            var messageEndReached = false
            do {
                val line = lineIterator.next()
                if (line == persistDivider.divider) {
                    messageEndReached = true
                } else {
                    body.appendln(line)
                }
            } while (lineIterator.hasNext() && !messageEndReached)
            return SimpleTrafficItem(TrafficMetadata(""), body.toString())
        }
        return null
    }
}