package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader

import java.util.stream.Stream

class CachedPoorTrafficReader(
    private val decorated: PoorTrafficReader
) : PoorTrafficReader {

    override suspend fun getUpTrafficStream(filter: ReadTrafficFilter): Stream<SimpleTrafficItem?> {
        // Is this already correct place for cache? Isnt it in higher place?
        // TODO d.bydzovsky cache saved traffic
        return decorated.getUpTrafficStream(filter)
    }

    override suspend fun getDownTrafficStream(filter: ReadTrafficFilter): Stream<SimpleTrafficItem?> {
        // Is this already correct place for cache? Isnt it in higher place?
        // TODO d.bydzovsky cache saved traffic
        return decorated.getDownTrafficStream(filter)
    }
}