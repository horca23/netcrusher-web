package com.horcacorp.testing.netcrusherweb.proxy.impl

import com.horcacorp.testing.netcrusherweb.event.api.EventBus
import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficDirection
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyModel
import com.horcacorp.testing.netcrusherweb.proxy.impl.filter.ChainingTransformFilter
import com.horcacorp.testing.netcrusherweb.proxy.impl.filter.CloningTransformFilter
import com.horcacorp.testing.netcrusherweb.proxy.impl.filter.TrafficItemNotifierFilter
import com.horcacorp.testing.netcrusherweb.proxy.impl.throttling.ConfigurableThrottler
import com.horcacorp.testing.netcrusherweb.persistance.api.PersistFilenameFactory
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.PersistorSwitcherImpl
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.monitor.SpeedMonitor
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.TrafficReaderFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.netcrusher.core.reactor.NioReactor
import org.netcrusher.tcp.TcpCrusherBuilder
import java.net.InetSocketAddress

class NetcrusherFactory(
    private val eventBus: EventBus,
    private val trafficReaderFactory: TrafficReaderFactory,
    private val filenameFactory: PersistFilenameFactory
) {

    fun buildNetcrusher(model: ProxyModel): Netcrusher {
        val connection = model.connection
        val reactor = NioReactor()
        val upFileStorage = filenameFactory.getUploadTrafficFile(connection.bindPort)
        val downFileStorage = filenameFactory.getDownloadTrafficFile(connection.bindPort)
        // todo select correct scope
        val notifierScope = CoroutineScope(Dispatchers.Main)
        val upNotifierFilter = TrafficItemNotifierFilter(
            connection.bindPort,
            ProxyTrafficDirection.UP,
            eventBus,
            notifierScope,
            model.persistCommunication
        )
        val downNotifierFilter = TrafficItemNotifierFilter(
            connection.bindPort,
            ProxyTrafficDirection.DOWN,
            eventBus,
            notifierScope,
            model.persistCommunication
        )
        val persistorSwitcher = PersistorSwitcherImpl(upNotifierFilter, downNotifierFilter)
        val downThrottler = ConfigurableThrottler(model.downloadThrottling)
        val upThrottler = ConfigurableThrottler(model.uploadThrottling)
        val trafficReader = trafficReaderFactory.createReader(upFile = upFileStorage, downFile = downFileStorage)
        val speedMonitor = SpeedMonitor(eventBus, connection.bindPort)
        val crusher = TcpCrusherBuilder.builder()
            .withReactor(reactor)
            .withBindAddress(InetSocketAddress(connection.bindPort))
            .withConnectAddress(InetSocketAddress(connection.connectHost, connection.connectPort))
            .withIncomingThrottlerFactory { downThrottler }
            .withOutgoingThrottlerFactory { upThrottler }
            .withIncomingTransformFilterFactory {
                ChainingTransformFilter(
                    speedMonitor.downMonitorFilter,
                    CloningTransformFilter(downNotifierFilter)
                )
            }
            .withOutgoingTransformFilterFactory {
                ChainingTransformFilter(
                    speedMonitor.upMonitorFilter,
                    CloningTransformFilter(upNotifierFilter)
                )
            }
            .build()
        return Netcrusher(
            reactor,
            crusher,
            downThrottler,
            upThrottler,
            speedMonitor,
            trafficReader,
            persistorSwitcher
        )
    }

}