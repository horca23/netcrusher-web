package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.monitor

import com.horcacorp.testing.netcrusherweb.event.api.EventBus
import com.horcacorp.testing.netcrusherweb.event.api.ProxySpeedReport
import com.horcacorp.testing.netcrusherweb.proxy.impl.filter.DownloadSpeedMonitoringFilter
import com.horcacorp.testing.netcrusherweb.proxy.impl.filter.UploadSpeedMonitoringFilter
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean

private const val MONITOR_INTERVAL_MILLIS = 1 * 1000L

class SpeedMonitor(private val eventBus: EventBus, private val bindPort: Int) {

    private val monitoring = AtomicBoolean(true)

    val downMonitorFilter: DownloadSpeedMonitoringFilter
    val upMonitorFilter: UploadSpeedMonitoringFilter

    init {
        val monitoringScope = CoroutineScope(Dispatchers.SpeedMonitor)
        val monitorChannel = Channel<MonitorMessage>()
        downMonitorFilter = DownloadSpeedMonitoringFilter(monitorChannel, monitoringScope)
        upMonitorFilter = UploadSpeedMonitoringFilter(monitorChannel, monitoringScope)
        monitoringScope.launch {
            try {
                launchMonitoringActor(monitorChannel)
                while (monitoring.get()) {
                    collectData(monitorChannel)
                        .also(::broadcastData)
                    clearData(monitorChannel)
                    delay(MONITOR_INTERVAL_MILLIS)
                }
            } finally {
                monitorChannel.close()
            }
        }
    }

    fun stopMonitoring() {
        monitoring.set(false)
    }

    private fun CoroutineScope.launchMonitoringActor(monitorChannel: Channel<MonitorMessage>) = launch {
        var downCounter = 0
        var upCounter = 0
        for (msg in monitorChannel) {
            when (msg) {
                is IncreaseDownload -> downCounter += msg.bytes
                is IncreaseUpload -> upCounter += msg.bytes
                is ClearMonitor -> {
                    downCounter = 0
                    upCounter = 0
                }
                is GetSpeed -> msg.response.complete(Speed(downCounter to upCounter))
            }
        }
    }

    private suspend fun collectData(monitorChannel: Channel<MonitorMessage>): Speed {
        return CompletableDeferred<Speed>()
            .also { monitorChannel.send(GetSpeed(it)) }
            .await()
    }

    private suspend fun clearData(monitorChannel: Channel<MonitorMessage>) {
        monitorChannel.send(ClearMonitor)
    }

    private fun broadcastData(speed: Speed) {
        eventBus.broadcast(ProxySpeedReport(bindPort, speed.down, speed.up))
    }
}

private val Dispatchers.SpeedMonitor by lazy { Executors.newSingleThreadExecutor().asCoroutineDispatcher() }

sealed class MonitorMessage
class IncreaseDownload(val bytes: Int) : MonitorMessage()
class IncreaseUpload(val bytes: Int) : MonitorMessage()
object ClearMonitor : MonitorMessage()
class GetSpeed(val response: CompletableDeferred<Speed>) : MonitorMessage()
inline class Speed(private val speed: Pair<Int, Int>) {
    val down get() = speed.first
    val up get() = speed.second
}