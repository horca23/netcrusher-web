package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.writer

import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficItemPassed
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.PersistTrafficDivider
import java.io.File
import java.io.FileOutputStream
import java.nio.charset.Charset

class SimpleTrafficWriter: TrafficWriter {

    private val divider = PersistTrafficDivider().divider

    override fun write(file: File, proxyItem: ProxyTrafficItemPassed) {
        val writer = FileOutputStream(file, true)
            .bufferedWriter(Charset.forName("utf-8"))
        writer.use {
            it.write(proxyItem.item.body)
            it.newLine()
            it.write(divider)
            it.flush()
        }
    }
}