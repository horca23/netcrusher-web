package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.writer

import com.horcacorp.testing.netcrusherweb.event.api.EventBus
import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficDirection.*
import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficItemPassed
import com.horcacorp.testing.netcrusherweb.persistance.api.ManagedFilesMutexPool
import com.horcacorp.testing.netcrusherweb.proxy.api.TrafficPersistor
import com.horcacorp.testing.netcrusherweb.persistance.api.PersistFilenameFactory

class TrafficPersistorImpl(
    private val managedFilesMutexPool: ManagedFilesMutexPool,
    private val eventBus: EventBus,
    private val filenameFactory: PersistFilenameFactory
) : TrafficPersistor {

    // todo d.bydzovsky Performance: implement buffered writer..
    private val writer: TrafficWriter = RollingTrafficWriter(SimpleTrafficWriter())

    override fun initialize() {
        eventBus.subscribe(ProxyTrafficItemPassed::class, ::processItem)
    }

    fun destroy() {
        eventBus.unsubscribe(ProxyTrafficItemPassed::class, ::processItem)
    }

    private suspend fun processItem(item: ProxyTrafficItemPassed) {
        if (item.persist) {
            val file = when (item.direction) {
                UP -> filenameFactory.getUploadTrafficFile(item.bindPort)
                DOWN -> filenameFactory.getDownloadTrafficFile(item.bindPort)
            }
            managedFilesMutexPool.borrowAndReturn(file) { writer.write(file, item) }
        }
    }
}