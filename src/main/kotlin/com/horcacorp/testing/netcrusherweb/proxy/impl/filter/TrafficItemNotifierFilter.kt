package com.horcacorp.testing.netcrusherweb.proxy.impl.filter

import com.horcacorp.testing.netcrusherweb.event.api.EventBus
import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficDirection
import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficItemPassed
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.SimpleTrafficItem
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.TrafficMetadata
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.nio.charset.Charset
import kotlinx.coroutines.launch
import org.netcrusher.core.filter.TransformFilter
import java.nio.ByteBuffer
import java.util.concurrent.atomic.AtomicBoolean

class TrafficItemNotifierFilter(
    private val proxyBindPort: Int,
    private val proxyDirection: ProxyTrafficDirection,
    private val eventBus: EventBus,
    private val scope: CoroutineScope,
    shouldPersist: Boolean): TransformFilter {

    private val persisting =  AtomicBoolean(shouldPersist)

    override fun transform(bb: ByteBuffer){
        val arr = ByteArray(bb.remaining())
        bb.get(arr)
        scope.launch(Dispatchers.Default) {
            val body = String(arr, Charset.forName("utf-8"))
            val metadata = TrafficMetadata("")
            val item = SimpleTrafficItem(metadata, body)
            eventBus.broadcast(ProxyTrafficItemPassed(proxyBindPort, proxyDirection, item, persisting.get()))
        }
    }

    fun turnOnCommunicationPersisting() {
        persisting.set(true)
    }

    fun turnOffCommunicationPersisting() {
        persisting.set(false)
    }
}