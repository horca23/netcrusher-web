package com.horcacorp.testing.netcrusherweb.proxy.impl.filter

import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.monitor.IncreaseDownload
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.monitor.MonitorMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import org.netcrusher.core.filter.TransformFilter
import java.nio.ByteBuffer

class DownloadSpeedMonitoringFilter(
    private val monitor: SendChannel<MonitorMessage>,
    private val scope: CoroutineScope
) : TransformFilter {

    override fun transform(bb: ByteBuffer) {
        val bytes = bb.remaining()
        scope.launch { monitor.send(IncreaseDownload(bytes)) }
    }
}