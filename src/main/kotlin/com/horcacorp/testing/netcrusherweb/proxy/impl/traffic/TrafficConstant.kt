package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic

class TrafficConstant {

    companion object {
        const val threshold = 20_000_000 // bytes
        const val fileCount = 10
    }
}