package com.horcacorp.testing.netcrusherweb.proxy.impl.filter

import org.netcrusher.core.filter.TransformFilter
import java.nio.ByteBuffer

class ChainingTransformFilter(private vararg val filters: TransformFilter) : TransformFilter {

    override fun transform(bb: ByteBuffer) {
        filters.forEach { it.transform(bb) }
    }
}