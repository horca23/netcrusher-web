package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic

import com.horcacorp.testing.netcrusherweb.proxy.api.PersistorSwitcher
import com.horcacorp.testing.netcrusherweb.proxy.impl.filter.TrafficItemNotifierFilter

class PersistorSwitcherImpl(vararg notifierFilter: TrafficItemNotifierFilter): PersistorSwitcher {

    private val filters = notifierFilter.toList()

    override fun enable() = filters.forEach {
        it.turnOnCommunicationPersisting()
    }

    override fun disable() = filters.forEach {
        it.turnOffCommunicationPersisting()
    }
}