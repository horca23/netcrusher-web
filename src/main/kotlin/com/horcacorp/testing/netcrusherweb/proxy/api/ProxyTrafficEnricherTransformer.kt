package com.horcacorp.testing.netcrusherweb.proxy.api

interface ProxyTrafficEnricherTransformer {

    /**
     * Start listen to eventBus and transform TrafficItemPass to RichTrafficItemPass
     */
    fun initialize()

    fun destroy()
}