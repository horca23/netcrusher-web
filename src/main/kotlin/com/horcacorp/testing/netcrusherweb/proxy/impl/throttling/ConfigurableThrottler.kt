package com.horcacorp.testing.netcrusherweb.proxy.impl.throttling

import com.horcacorp.testing.netcrusherweb.proxy.api.ThrottlingConfig
import com.horcacorp.testing.netcrusherweb.proxy.api.ThrottlingMode
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.netcrusher.core.throttle.DelayThrottler
import org.netcrusher.core.throttle.Throttler
import org.netcrusher.core.throttle.rate.ByteRateThrottler
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

class ConfigurableThrottler(initConfig: ThrottlingConfig) : Throttler {

    private val configActorChannel = Channel<ThrottlingConfigMsg>()

    init {
        CoroutineScope(Dispatchers.Default).launchConfigActorCoroutine(initConfig)
    }

    override fun calculateDelayNs(bb: ByteBuffer) = runBlocking {
        CompletableDeferred<Throttler>()
            .also { configActorChannel.send(GetThrottlingConfigMsg(it)) }
            .await()
            .calculateDelayNs(bb)
    }

    suspend fun configureThrottler(newConfig: ThrottlingConfig) {
        configActorChannel.send(UpdateThrottlingConfigMsg(newConfig))
    }

    private fun createThrottler(config: ThrottlingConfig) = when (config.mode) {
        ThrottlingMode.OFF -> Throttler.NOOP
        ThrottlingMode.BYTE_RATE -> {
            require(config.value > 0)
            ByteRateThrottler(config.value, 1, TimeUnit.SECONDS)
        }
        ThrottlingMode.DELAY -> {
            require(config.value > 0)
            DelayThrottler(config.value, TimeUnit.MILLISECONDS)
        }
    }

    private fun CoroutineScope.launchConfigActorCoroutine(initConfig: ThrottlingConfig) =
        launch {
            var throttler = createThrottler(initConfig)
            for (msg in configActorChannel) {
                when (msg) {
                    is UpdateThrottlingConfigMsg -> throttler = createThrottler(msg.newConfig)
                    is GetThrottlingConfigMsg -> msg.response.complete(throttler)
                }
            }
        }
}

private sealed class ThrottlingConfigMsg
private class UpdateThrottlingConfigMsg(val newConfig: ThrottlingConfig) : ThrottlingConfigMsg()
private class GetThrottlingConfigMsg(val response: CompletableDeferred<Throttler>) : ThrottlingConfigMsg()