package com.horcacorp.testing.netcrusherweb.proxy.api

interface ProxyHistoryWriter {

    fun initialize()

    fun destroy()

}