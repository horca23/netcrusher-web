package com.horcacorp.testing.netcrusherweb.proxy.impl

import com.horcacorp.testing.netcrusherweb.proxy.api.*
import com.horcacorp.testing.netcrusherweb.proxy.impl.throttling.ConfigurableThrottler
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.monitor.SpeedMonitor
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.ReadTrafficFilter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.netcrusher.core.reactor.NioReactor
import org.netcrusher.tcp.TcpCrusher

class Proxy private constructor(val model: ProxyModel, private val netcrusher: Netcrusher) {

    constructor(model: ProxyModel, factory: NetcrusherFactory) : this(
        model,
        when (model.state) {
            ProxyState.OPEN -> factory.buildNetcrusher(model).apply { open() }
            ProxyState.CLOSED -> factory.buildNetcrusher(model)
            ProxyState.FROZEN -> factory.buildNetcrusher(model).apply { open(); freeze() }
        }
    )

    val bindPort = model.connection.bindPort

    fun open() = when (model.state) {
        ProxyState.OPEN -> this
        ProxyState.CLOSED -> Proxy(
            model.copy(state = ProxyState.OPEN),
            netcrusher.apply { open() }
        )
        ProxyState.FROZEN -> Proxy(
            model.copy(state = ProxyState.OPEN),
            netcrusher.apply { unfreeze() }
        )
    }

    fun close() = when (model.state) {
        ProxyState.OPEN -> Proxy(
            model.copy(state = ProxyState.CLOSED),
            netcrusher.apply { close() }
        )
        ProxyState.CLOSED -> this
        ProxyState.FROZEN -> Proxy(
            model.copy(state = ProxyState.CLOSED),
            netcrusher.apply { close() }
        )
    }

    fun freeze() = when (model.state) {
        ProxyState.OPEN -> Proxy(
            model.copy(state = ProxyState.FROZEN),
            netcrusher.apply { freeze() }
        )
        ProxyState.CLOSED -> Proxy(
            model.copy(state = ProxyState.FROZEN),
            netcrusher.apply { open(); freeze() }
        )
        ProxyState.FROZEN -> this
    }

    fun editDescription(newDescription: String) = Proxy(
        model.copy(description = newDescription),
        netcrusher
    )

    suspend fun editThrottling(newDownConfig: ThrottlingConfig, newUpConfig: ThrottlingConfig) = Proxy(
        model.copy(downloadThrottling = newDownConfig, uploadThrottling = newUpConfig),
        netcrusher.apply { configureThrottling(newDownConfig, newUpConfig) }
    )

    fun turnOnCommunicationPersisting() = Proxy(
        model.copy(persistCommunication = true),
        netcrusher.apply { turnOnCommunicationPersisting() }
    )

    fun turnOffCommunicationPersisting() = Proxy(
        model.copy(persistCommunication = false),
        netcrusher.apply { turnOffCommunicationPersisting() }
    )

    fun destroy() {
        netcrusher.destroy()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Proxy) return false

        if (bindPort != other.bindPort) return false

        return true
    }

    override fun hashCode() = bindPort
}

class Netcrusher(
    private val reactor: NioReactor,
    private val crusher: TcpCrusher,
    private val downloadThrottler: ConfigurableThrottler,
    private val uploadThrottler: ConfigurableThrottler,
    private val speedMonitor: SpeedMonitor,
    private val trafficReader: TrafficReader,
    private val persistorSwitcher: PersistorSwitcher
) {

    fun open() {
        crusher.open()
    }

    fun close() {
        crusher.close()
    }

    fun freeze() {
        crusher.freeze()
    }

    fun unfreeze() {
        crusher.unfreeze()
    }

    suspend fun getUpTraffic() = withContext(Dispatchers.IO) { trafficReader.readUpTraffic(ReadTrafficFilter()) }

    suspend fun getDownTraffic() = withContext(Dispatchers.IO) { trafficReader.readDownTraffic(ReadTrafficFilter()) }


    suspend fun configureThrottling(newDownConfig: ThrottlingConfig, newUpConfig: ThrottlingConfig) {
        downloadThrottler.configureThrottler(newDownConfig)
        uploadThrottler.configureThrottler(newUpConfig)
    }

    fun turnOnCommunicationPersisting() = persistorSwitcher.enable()

    fun turnOffCommunicationPersisting() = persistorSwitcher.disable()

    fun destroy() {
        crusher.close()
        reactor.close()
        speedMonitor.stopMonitoring()
    }
}