package com.horcacorp.testing.netcrusherweb.proxy.impl.filter

import org.netcrusher.core.filter.TransformFilter
import java.nio.ByteBuffer

class CloningTransformFilter(private val decorated: TransformFilter) : TransformFilter {

    override fun transform(bb: ByteBuffer) {
        decorated.transform(bb.clone())
    }

    private fun ByteBuffer.clone(): ByteBuffer {
        val clone = ByteBuffer.allocate(capacity())
        rewind()
        clone.put(this)
        rewind()
        clone.flip()
        return clone
    }
}