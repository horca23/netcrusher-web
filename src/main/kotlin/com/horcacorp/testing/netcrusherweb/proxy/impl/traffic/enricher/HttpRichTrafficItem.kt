package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher
 import com.horcacorp.testing.netcrusherweb.proxy.api.EnrichedTrafficItem
 import com.horcacorp.testing.netcrusherweb.proxy.api.TrafficType

class HttpRichTrafficItem : EnrichedTrafficItem {
    override val type = TrafficType.HTTP
}