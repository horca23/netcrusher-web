package com.horcacorp.testing.netcrusherweb.event.api

import com.horcacorp.testing.netcrusherweb.proxy.api.EnrichedTrafficItem
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyModel
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.SimpleTrafficItem

class ProxyAdded(val proxyModel: ProxyModel) : Event
class ProxyOpened(val bindPort: Int) : Event
class ProxyClosed(val bindPort: Int) : Event
class ProxyFroze(val bindPort: Int) : Event
class ProxyRemoved(val bindPort: Int) : Event
class ProxyEdited(val bindPort: Int, val newProxyModel: ProxyModel) : Event
class ProxySpeedReport(val bindPort: Int, val bytesDown: Int, val bytesUp: Int) : Event
class ProxyTrafficItemPassed(val bindPort: Int,
                             val direction: ProxyTrafficDirection,
                             val item: SimpleTrafficItem,
                             val persist: Boolean): Event
class RichProxyTrafficItemPassed(val bindPort: Int, val direction: ProxyTrafficDirection, val item: EnrichedTrafficItem): Event
enum class ProxyTrafficDirection { UP, DOWN }