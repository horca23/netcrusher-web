package com.horcacorp.testing.netcrusherweb.event.impl

import com.horcacorp.testing.netcrusherweb.event.api.Event
import com.horcacorp.testing.netcrusherweb.event.api.EventBus
import kotlinx.coroutines.*
import java.util.concurrent.Executors
import kotlin.reflect.KClass

class EventBusImpl : EventBus {

    private val eventBusScope = CoroutineScope(Dispatchers.EventBus)
    private val subscribedActions: MutableMap<KClass<*>, MutableList<suspend (Event) -> Unit>> = mutableMapOf()

    @Suppress("UNCHECKED_CAST")
    override fun <T : Event> subscribe(event: KClass<T>, action: suspend (T) -> Unit) {
        subscribedActions[event] = subscribedActions[event]
            ?.also { it.add(action as suspend (Event) -> Unit) }
            ?: mutableListOf(action as suspend (Event) -> Unit)
    }

    override fun <T : Event> unsubscribe(event: KClass<T>, action: suspend (T) -> Unit) {
        subscribedActions[event]?.remove(action)
    }

    override fun broadcast(event: Event) {
        eventBusScope.launch {
            subscribedActions[event::class]?.forEach { it(event) }
        }
    }
}

private val Dispatchers.EventBus by lazy { Executors.newSingleThreadExecutor().asCoroutineDispatcher() }