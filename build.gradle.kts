import org.gradle.internal.os.OperatingSystem
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.72"
    application
}

group = "com.horcacorp.testing"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(group = "org.slf4j", name = "slf4j-simple", version = "1.7.30")
    implementation(group = "io.ktor", name = "ktor-server-netty", version = "1.3.1")
    implementation(group = "io.ktor", name = "ktor-jackson", version = "1.3.1")
    implementation(group = "io.ktor", name = "ktor-websockets", version = "1.3.1")
    implementation(group = "com.github.netcrusherorg", name = "netcrusher-core", version = "0.10")
    implementation(group = "com.fasterxml.jackson.module", name = "jackson-module-kotlin", version = "2.11.0")
}

tasks.register<Exec>("installFrontendDependencies") {
    workingDir = rootDir.resolve("frontend")
    if (!workingDir.resolve("node_modules").exists()) {
        cmd("npm", "install")
    } else {
        cmd("echo", "Frontend dependencies already installed. Skipping.")
    }
}

tasks.register<Exec>("buildFrontend") {
    workingDir = rootDir.resolve("frontend")
    cmd("npm", "run", "build")
    dependsOn("installFrontendDependencies")
}

tasks.register<Copy>("copyFrontend") {
    from("frontend/build")
    into("src/main/resources")
    dependsOn("buildFrontend")
}

tasks.getByName("build").dependsOn("copyFrontend")

tasks.register<Delete>("cleanFrontend") {
    delete("frontend/build", "frontend/node_modules")
}

tasks.getByName("clean").dependsOn("cleanFrontend")

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application.mainClassName = "com.horcacorp.testing.netcrusherweb.webserver.NetcrusherWebApplication"

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    freeCompilerArgs = listOf("-XXLanguage:+InlineClasses")
}

fun Exec.cmd(vararg args: Any) {
    if (OperatingSystem.current().isWindows) {
        commandLine("cmd", "/c", *args)
    } else {
        commandLine(*args)
    }
}